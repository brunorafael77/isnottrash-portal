/**
 * Created by Bruno Rafal on 13/02/2016.
 */
(function(){
  app.controller('RegisterUserCtrl',

      function($scope, $translate){

        $translate('USER_DATA').then(function (user_data) {
          $scope.USER_DATA = user_data;
        });
        $translate('USER_CONFIRM_EMAIL').then(function (user_confirm_email) {
          $scope.USER_CONFIRM_EMAIL = user_confirm_email;
        });
        $translate('USER_EMAIL_ENTRY').then(function (user_email_entry) {
          $scope.USER_EMAIL_ENTRY = user_email_entry;
        });
        $translate('CANCEL').then(function (cancel) {
          $scope.CANCEL = cancel;
        });
        $translate('SEND').then(function (send) {
          $scope.SEND = send;
        });
        $translate('INVALID_DATA').then(function (invalid_data) {
          $scope.INVALID_DATA = invalid_data;
        });
        $translate('ERR_CONNECTION').then(function (err_connection) {
          $scope.ERR_CONNECTION = err_connection;
        });
        $translate('USER_CREATE').then(function (user_create) {
          $scope.USER_CREATE = user_create;
        });
        $translate('USER_NOT_CREATE').then(function (user_not_create) {
          $scope.USER_NOT_CREATE = user_not_create;
        });
        $translate('CONFLIT_EMAIL').then(function (conflit_email) {
          $scope.CONFLIT_EMAIL = conflit_email;
        });

        var popup;
        $scope.createRegisterUserPopup = function(){
          /*$scope.newUser = {};
          popup = $ionicPopup.show({
            templateUrl: 'registerUserPopup.html',
            title: $scope.CREATE_ACCOUNT,
            subTitle: $scope.USER_DATA,
            scope: $scope
          });*/
        };

        $scope.createSendPasswordPopup = function() {
          $scope.info = {};
          /*$ionicPopup.show({
            template: '<input type="email" ng-model="info.email" type="email" placeholder="example@mail.com" required>',
            title: $scope.USER_CONFIRM_EMAIL,
            subTitle: $scope.USER_EMAIL_ENTRY,
            scope: $scope,
            buttons: [
              { text:  $scope.CANCEL },
              {
                text: '<b>' + $scope.SEND + '</b>',
                type: 'button-positive',
                onTap: function(e) {
                  if (!$scope.info.email) {
                    $scope.showAlert($scope.INVALID_DATA);
                    e.preventDefault();
                  } else {
                    console.log($scope.info);
                    $scope.sendEmail({'email': $scope.info.email});
                  }
                }
              }]
          });*/
        };

        $scope.sendEmail = function(email) {
          /*UserService.recoverPass(email).then(function(result){
            console.log("enviando email para: " + email);
          }).catch(function(erro){
            console.log(erro);
            $scope.showAlert($scope.ERR_CONNECTION);
          });*/
        };

        $scope.cancel = function() {
          popup.close();
        };

        $scope.focus = function(campo) {
          campo.$focus=true;
          campo.$blur=false;
        };

        $scope.blur = function(campo) {
          campo.$focus=false;
          campo.$blur=true;
        };

        $scope.showAlert = function(message) {
          /*var alertPopup = $ionicPopup.alert({
            title: message,
          });*/
        };
      }
  );
})();
