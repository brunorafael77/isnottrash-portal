/**
 * Created by Bruno Rafael on 04/02/2016.
 */
(function() {
  app.controller('LoginCtrl',
    ['$scope', '$rootScope', '$translate',
      function($scope, $rootScope,  $translate){

        //remove in production
        $scope.loginUser = {};
        $scope.loginUser.login = 'bruno@mail.com';
        $scope.loginUser.senha = '123456';

        $translate('ERR').then(function (err) {
          $scope.ERR = err;
        });
        $translate('ERR_AUTHENTICATION').then(function (err_authentication) {
          $scope.ERR_AUTHENTICATION = err_authentication;
        });
        $translate('INVALID_FIELDS').then(function (invalid_fields) {
          $scope.INVALID_FIELDS = invalid_fields;
        });

        $(function() {
          var userData = localStorage.getItem('usuarioLogado');
          if (localStorage.getItem('manterLogado') == true && userData) {
            $rootScope.loggedUser = userData;
            $state.go('tab.timeline');
            WebSocketService.connect();
          }
        });

        $scope.login = function(){
          $rootScope.goTo('/home');
          /*if($scope.verificaCampos(loginForm)){
            $ionicPopup.alert({title: $scope.INVALID_FIELDS});
          } else {
            $scope.authenticating = true;
            var user = {
              email:loginUser.login,
              password:loginUser.senha
            };
            UserService.login(user).then(function(result){
              if(result.data.success){
                if (document.getElementById("manterLogado").checked) {
                  localStorage.setItem('manterLogado',true);
                }
                var userData = result.data.userInformations;
                userData.token= result.data.token;
                localStorage.setItem('usuarioLogado', userData);
                $rootScope.loggedUser = userData;
                $rootScope.myCroppedImage = $rootScope.loggedUser.photo;
                $translate.use(userData.settings.language);
                $state.go('home');
                $scope.authenticating = false;
                WebSocketService.connect($rootScope.loggedUser.token);
              } else {
                console.log(result);
                $ionicPopup.alert({title: $scope.ERR_AUTHENTICATION, template: '<div class="text-center">{{"INVALID_LOGIN" | translate}}</div>'});
                $scope.authenticating = false;
              }
            }).catch( function(result){
              $scope.authenticating = false;
              if(!status && !result.data){
                $ionicPopup.alert({title: $scope.ERR_AUTHENTICATION, template: '<div class="text-center">{{"ERR_CONNECTION" | translate}}</div>'});
              } else {
                console.log(result);
                $ionicPopup.alert({title: $scope.ERR, template: '<div class="text-center">{{"ERR_LOGIN" | translate}}</div>'});

              }

            });
          }*/
        };

        $scope.verificaCampos = function(login) {
          return login.$invalid;
        };
      }
    ]
  );
})();
